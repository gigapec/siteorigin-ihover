<?php
/*
Widget Name: iHover
Description: A very simple SiteOrigin iHover widget.
Author: Jean-Philippe Garcia Ballester
*/

define('SOW_IHOVER_VERSION', '0.0.1');

class SiteOrigin_iHover_Widget extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'sow-ihover',
			__('SiteOrigin iHover'),
			array(
				'description' => __('A simple iHover widget.')
			),
			array(

			),
			false,
			plugin_dir_path(__FILE__)
		);
	}

  function initialize(){
    $this->register_frontend_styles(
      array(
        array(
          'sow-ihover-main',
          plugin_dir_url(__FILE__) . 'css/style.css',
        )
      )   
    ); 
    $this->register_frontend_scripts(
      array(
        array(
          'sow-ihover-main',
          plugin_dir_url(__FILE__) . 'js/ihover.js',
          array( 'jquery' ),
        )
      )   
    ); 
  }

	function initialize_form(){

		global $_wp_additional_image_sizes;
		$sizes = array(
			'full' => __('Full', 'so-widgets-bundle'),
			'large' => __('Large', 'so-widgets-bundle'),
			'medium' => __('Medium', 'so-widgets-bundle'),
			'thumb' => __('Thumbnail', 'so-widgets-bundle'),
		);
		if( !empty($_wp_additional_image_sizes) ) {
			foreach($_wp_additional_image_sizes as $i => $s) {
				$sizes[$i] = $i;
			}
		}

		return array(
			'image' => array(
				'type' => 'media',
				'label' => __('Image file', 'so-widgets-bundle'),
				'library' => 'image',
				'fallback' => true,
			),

			'size' => array(
				'type' => 'select',
				'label' => __('Image size', 'so-widgets-bundle'),
				'options' => $sizes,
			),

			'align' => array(
				'type' => 'select',
				'label' => __('Image alignment', 'so-widgets-bundle'),
				'default' => 'default',
				'options' => array(
					'default' => __('Default', 'so-widgets-bundle'),
					'left' => __('Left', 'so-widgets-bundle'),
					'right' => __('Right', 'so-widgets-bundle'),
					'center' => __('Center', 'so-widgets-bundle'),
				),
			),

			'title' => array(
				'type' => 'text',
				'label' => __('Title text', 'so-widgets-bundle'),
			),

			'description' => array(
				'type' => 'textarea',
				'label' => __('Description text', 'so-widgets-bundle'),
			),

			'alt' => array(
				'type' => 'text',
				'label' => __('Alt text', 'so-widgets-bundle'),
			),

			'url' => array(
				'type' => 'link',
				'label' => __('Destination URL', 'so-widgets-bundle'),
			),
			'new_window' => array(
				'type' => 'checkbox',
				'default' => false,
				'label' => __('Open in new window', 'so-widgets-bundle'),
			),

			'bound' => array(
				'type' => 'checkbox',
				'default' => true,
				'label' => __('Bound', 'so-widgets-bundle'),
				'description' => __("Make sure the image doesn't extend beyond its container.", 'so-widgets-bundle'),
			),
			'full_width' => array(
				'type' => 'checkbox',
				'default' => false,
				'label' => __('Full Width', 'so-widgets-bundle'),
				'description' => __("Resize image to fit its container.", 'so-widgets-bundle'),
			),

		);
	}

	function get_style_hash($instance) {
		return substr( md5( serialize( $this->get_less_variables( $instance ) ) ), 0, 12 );
	}

	function get_template_name($instance) {
		return 'base';
	}

	public function get_template_variables( $instance, $args ) {
		return array(
			'title' => $instance['title'],
			'description' => $instance['description'],
			'image' => $instance['image'],
			'size' => $instance['size'],
			'image_fallback' => ! empty( $instance['image_fallback'] ) ? $instance['image_fallback'] : false,
			'alt' => $instance['alt'],
			'url' => $instance['url'],
			'new_window' => $instance['new_window'],
		);
	}

	function get_style_name($instance) {
		return 'sow-ihover';
	}

	function get_less_variables($instance){
		return array(
			'image_alignment' => $instance['align'],
			'image_display' => $instance['align'] == 'default' ? 'block' : 'inline-block',
			'image_max_width' => ! empty( $instance['bound'] ) ? '100%' : '',
			'image_height' => ! empty( $instance['bound'] ) ? 'auto' : '',
			'image_width' => ! empty( $instance['full_width'] ) ? '100%' : '',
		);
	}
}

siteorigin_widget_register('sow-ihover', __FILE__, 'SiteOrigin_iHover_Widget');
