<?php
/**
 * @var $title
 * @var $description
 * @var $image
 * @var $size
 * @var $image_fallback
 * @var $alt
 * @var $url
 * @var $new_window
 */
?>

<?php
$src = siteorigin_widgets_get_attachment_image_src(
	$image,
	$size,
	$image_fallback
);

$attr = array();
if( !empty($src) ) {
	$attr = array(
		'src' => $src[0],
	);

	if(!empty($src[1])) $attr['width'] = $src[1];
	if(!empty($src[2])) $attr['height'] = $src[2];
	if (function_exists('wp_get_attachment_image_srcset')) {
		$attr['srcset'] = wp_get_attachment_image_srcset($image, $size);
 	}
}

if(!empty($title)) $attr['title'] = $title;
if(!empty($alt)) $attr['alt'] = $alt;

?>
<div class="ih-item square effect6 from_top_and_bottom">
  <a href="<?php echo sow_esc_url($url) ?>" <?php if($new_window) echo 'target="_blank"' ?>>
    <div class="img">
	    <img <?php foreach($attr as $n => $v) echo $n.'="' . esc_attr($v) . '" ' ?> />
    </div>
    <div class="info">
      <p class="title"><?php echo $title; ?></p>
      <p><?php echo $description; ?></p>
    </div>
  </a>
</div>
