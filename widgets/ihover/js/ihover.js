var $j = jQuery.noConflict();
$j(function() {
  $j('.ih-item a').on('touchstart', function (e) {
    'use strict'; //satisfy code inspectors
    var link = $j(this); //preselect the link
    if (link.hasClass('hover')) {
      return true;
    } else {
      link.addClass('hover');
      $j('.ih-item a').not(this).removeClass('hover');
      e.preventDefault();
      return false; //extra, and to make sure the function has consistent return points
    }
  });
});
