<?php

function siteorigin-ihover( $widget_folders ) {
    array_push($widget_folders, plugin_dir_path(__FILE__).'widgets/');

    return $widget_folders;
}
add_filter( 'siteorigin_widgets_widget_folders', 'siteorigin-ihover', 10, 3 );
